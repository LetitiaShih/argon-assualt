﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class audioPlayer : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		int numMusicPlayer=FindObjectsOfType<audioPlayer>().Length;
		if(numMusicPlayer>1)
		{
			Destroy(gameObject);
		}
		else{
			DontDestroyOnLoad(gameObject);
		}
	}
	
	// Update is called once per frame

}
