﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[SerializeField]GameObject deathFX;
	[SerializeField] int scorePerHit=12;
	[SerializeField] int hits=10;//shoot a sec to be destroyed(partical generation rate=10)
	ScoreBoard scoreBoard;
	void Start () {
		scoreBoard=FindObjectOfType<ScoreBoard>();
		Collider boxCollider=gameObject.AddComponent<BoxCollider>();
		boxCollider.isTrigger=false;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnParticleCollision(GameObject other)
	{
		ProcessHit();
		if(hits<=0)
		{
			scoreBoard.AddScore(scorePerHit);
			killEnemy();
		}

	}
	void ProcessHit()
	{
		hits--;
	}
	void killEnemy()
	{
		Instantiate(deathFX,transform.position,Quaternion.identity);
		Destroy(gameObject);
	}
}
