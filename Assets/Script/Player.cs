﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {


	[Tooltip("In ms^-1(m per sec)")][SerializeField]float Speed=10f;
	[Tooltip("In m")][SerializeField]float xRange=6f;
	[Tooltip("In m")][SerializeField]float yRange=4.5f;

	[SerializeField]float positionPitchFactor=-5f;
	[SerializeField]float controlPitchFactor=-20f;
	[SerializeField]float positionYawFactor=6.5f;
	[SerializeField]float controlRollFactor=-30f;
	float xThrow,yThrow;
	bool isPlayerEnabled=true; 
	[SerializeField]GameObject[] guns;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(isPlayerEnabled)
		{
			ProcessTranslation();
			ProcessRotation();	
			ProcessFire();		
		}

	}
	void OnPlayerDeath(){
		isPlayerEnabled=false;
	}
	private void ProcessRotation()
	{
		float pitchDueToPosition=transform.localPosition.y*positionPitchFactor;
		float pitchDueToControlThrow=yThrow*controlPitchFactor;
		float pitch=pitchDueToPosition+pitchDueToControlThrow;
		float yaw=transform.localPosition.x*positionYawFactor;
		float roll=xThrow*controlRollFactor;
		transform.localRotation=Quaternion.Euler(pitch,yaw,roll);
	}
	private void ProcessTranslation()
	{
		xThrow=CrossPlatformInputManager.GetAxis("Horizontal");
		float xOffset=xThrow*Speed*Time.deltaTime;
		float rawNewXPos=transform.localPosition.x+xOffset;
		float clampedXPos=Mathf.Clamp(rawNewXPos,-xRange,xRange);

		yThrow=CrossPlatformInputManager.GetAxis("Vertical");
		float yOffset=yThrow*Speed*Time.deltaTime;
		float rawNewYPos=transform.localPosition.y+yOffset;
		float clampedYPos=Mathf.Clamp(rawNewYPos,-yRange,yRange);
		transform.localPosition=new Vector3(clampedXPos,clampedYPos,transform.localPosition.z);
	}
	void ProcessFire()
	{
		if(CrossPlatformInputManager.GetButton("Fire"))
		{
			SetGunsActivate(true);
		}
		else
		{
			SetGunsActivate(false);
		}
	}
	void SetGunsActivate(bool isActive)
	{
		foreach(GameObject gun in guns)
		{
			var emissionModule=gun.GetComponent<ParticleSystem>().emission;
			emissionModule.enabled=isActive;
			//gun.SetActive(isActive);
		}
	}

}
