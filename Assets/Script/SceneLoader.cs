﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	void Start () {
		Invoke("LoadMainScene",4f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void LoadMainScene()
	{
		SceneManager.LoadScene("SampleScene");
	}
}
