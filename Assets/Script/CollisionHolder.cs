﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHolder : MonoBehaviour {

	[SerializeField]int loadDelay=1;
	[SerializeField]GameObject deathFX;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	 void OnTriggerEnter(Collider other)
	 {
		deathFX.SetActive(true);
		//print("collide with something");
		PlayerIsDied();
		Invoke("ReloadScene",loadDelay);

	 }
	 void PlayerIsDied()
	 {
		SendMessage("OnPlayerDeath");
	 }
	 void ReloadScene()
	 {
		SceneManager.LoadScene(1);
	 }
}
