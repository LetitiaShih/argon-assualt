﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

	int score=0;
	Text scoreText;
	void Start () {
		scoreText=GetComponent<Text>();
		scoreText.text=score.ToString();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void AddScore(int scoreIncrease)
	{
		score=score+scoreIncrease;
		scoreText.text=score.ToString();
	}
}
